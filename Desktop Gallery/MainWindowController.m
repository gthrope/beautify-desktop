//
//  MainWindowController.m
//  Desktop Gallery
//
//  Created by Glenn Thrope on 2/28/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import "MainWindowController.h"
#import "DescriptionWindowController.h"


@implementation MainWindowController

@synthesize descriptionWindowController = _descriptionWindowController;

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (DescriptionWindowController *) descriptionWindowController
{
    if(!_descriptionWindowController)
    {
        _descriptionWindowController = [[DescriptionWindowController alloc] initWithWindowNibName:@"DescriptionWindow"];
    }
    return _descriptionWindowController;
}

- (IBAction)login:(NSButton *)sender
{
    [self close];
    [self.descriptionWindowController showWindow:self];
}


#pragma mark PhFacebookDelegate methods

- (void) tokenResult: (NSDictionary*) result
{
    
}

- (void) requestResult: (NSDictionary*) result
{
    
}

- (void) willShowUINotification: (PhFacebook*) sender
{
    
}

- (void) didDismissUI: (PhFacebook*) sender
{
    
}

@end
