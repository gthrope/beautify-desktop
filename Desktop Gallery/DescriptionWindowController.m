//
//  DescriptionWindowController.m
//  Desktop Gallery
//
//  Created by Glenn Thrope on 2/28/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import "DescriptionWindowController.h"

@implementation DescriptionWindowController

@synthesize fb = _fb;
@synthesize artData = _artData;

@synthesize paintingName = _paintingName;
@synthesize artistName = _artistName;
@synthesize galleryName = _galleryName;
@synthesize likeButton = _likeButton;
@synthesize dislikeButton = _dislikeButton;
@synthesize browseButton = _browseButton;

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}


NSInteger count = 0;

- (ArtData *)artData
{
    if(!_artData)
    {
        _artData = [[ArtData alloc] init];
    }
    return _artData;
}

#pragma mark IBActions

- (IBAction)likeClicked:(NSButton *)sender
{
    // do nothing for now
}

- (IBAction)dislikeClicked:(NSButton *)sender
{
    count++;
    [self changeBackground];
}

- (IBAction)browseClicked:(NSButton *)sender
{
    NSURL *paintingUrl = [self.artData getPictureURL:count];
    [[NSWorkspace sharedWorkspace] openURL:paintingUrl];
}

/*
- (IBAction)setBackground:(NSButton *)sender
{
    [self.setBackgroundButton setHidden:YES];
    [self.paintingName setHidden:NO];
    [self.artistName setHidden:NO];
    [self.galleryName setHidden:NO];
    [self.likeButton setHidden:NO];
    [self.dislikeButton setHidden:NO];
    [self.browseButton setHidden:NO];
    
    // set up chrome extension
     NSError *error;
     BOOL success = [[NSFileManager defaultManager] copyItemAtPath:@"/Users/glennthrope/Documents/Developer/Desktop Gallery/Desktop Gallery/Extension" toPath:@"/Users/glennthrope/Library/Application Support/Google/Chrome/Default/Extensions" error:&error];
     
     if(!success)
     {
     NSLog(@"%@", [error localizedDescription]);
     }
    
     PhFacebook* fb = [[PhFacebook alloc] initWithApplicationID: @"584474728248905" delegate: self];
     [fb getAccessTokenForPermissions: [NSArray arrayWithObjects: @"publish_actions", nil] cached:YES];
 
    [self changeBackground];
}*/

- (void)changeBackground
{
    NSURL *backgroundUrl = [self.artData getPictureFile:count];
    NSString *paintingName = [self.artData getPictureName:count];
    NSString *artistName = [self.artData getArtistNameForPicture:count];
    NSString *galleryName = [self.artData getGalleryNameForPicture:count];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:NSImageScaleNone], NSWorkspaceDesktopImageScalingKey, nil];
    [[NSWorkspace sharedWorkspace] setDesktopImageURL:backgroundUrl forScreen:[NSScreen mainScreen] options:options error:nil];
    
    [self.paintingName setStringValue:[NSString stringWithFormat:@"\"%@\"", paintingName]];
    [self.artistName setStringValue:[NSString stringWithFormat:@"by %@", artistName]];
    [self.galleryName setStringValue:galleryName];
}

@end
