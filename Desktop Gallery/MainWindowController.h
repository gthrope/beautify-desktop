//
//  MainWindowController.h
//  Desktop Gallery
//
//  Created by Glenn Thrope on 2/28/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <PhFacebook/PhFacebook.h>
@class DescriptionWindowController;

@interface MainWindowController : NSWindowController <PhFacebookDelegate>

@property (strong, nonatomic) DescriptionWindowController *descriptionWindowController;

- (IBAction)login:(NSButton *)sender;

@end
