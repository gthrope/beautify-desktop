//
//  main.m
//  Desktop Gallery
//
//  Created by Glenn Thrope on 2/19/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
