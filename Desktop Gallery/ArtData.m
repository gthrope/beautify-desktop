//
//  ArtData.m
//  Desktop Gallery
//
//  Created by Glenn Thrope on 2/19/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import "ArtData.h"

#define NUM_BACKGROUNDS 10

@interface ArtData ()
@property (strong, nonatomic) NSMutableArray *data;
@end

@implementation ArtData

@synthesize data = _data;

-(NSMutableArray *)data
{
    if(!_data)
    {
        _data = [[NSMutableArray alloc] initWithCapacity:NUM_BACKGROUNDS];
        
        // format: [<picture name>, <picture url>, <artist name>]
        _data[0] = [NSArray arrayWithObjects:@"Don't Fight Back", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=1123", @"The Date Farmers", nil];
        _data[1] = [NSArray arrayWithObjects:@"We Become What We Are", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=1380", @"Cleon Peterson", nil];
        _data[2] = [NSArray arrayWithObjects:@"Generator", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=1356", @"David Ellis", nil];
        _data[3] = [NSArray arrayWithObjects:@"Mimi", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=1244", @"Deanna Templeton", nil];
        _data[4] = [NSArray arrayWithObjects:@"Lovely", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=591", @"Jeff Soto", nil];
        _data[5] = [NSArray arrayWithObjects:@"Bird Autopsy", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=903", @"Matt Furie", nil];
        _data[6] = [NSArray arrayWithObjects:@"Untitled", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=411", @"Taylor McKimens", nil];
        _data[7] = [NSArray arrayWithObjects:@"Red Dawn", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=1473", @"Suzannah Sinclair", nil];
        _data[8] = [NSArray arrayWithObjects:@"Mailboxes", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=264", @"Evan Hecox", nil];
        _data[9] = [NSArray arrayWithObjects:@"little flowers", @"http://artists.newimageartgallery.com/scripts/prodView.asp?idproduct=1506", @"Paul Wackers", nil];
    }
    return _data;
}

- (NSString *) getPictureName:(NSInteger)index
{
    return [[[self data] objectAtIndex:index % NUM_BACKGROUNDS] objectAtIndex:0];
}

- (NSURL *) getPictureURL:(NSInteger)index
{
    return [NSURL URLWithString:[[[self data] objectAtIndex:index % NUM_BACKGROUNDS] objectAtIndex:1]];
}

- (NSURL *) getPictureFile:(NSInteger)index
{
    return [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%ld", index % NUM_BACKGROUNDS] ofType:@"jpg" inDirectory:@"art"]];
}

- (NSString *) getArtistNameForPicture:(NSInteger)index
{
    return [[[self data] objectAtIndex:index % NUM_BACKGROUNDS] objectAtIndex:2];
}

/*
- (NSString *) getArtistURLForPicture:(NSInteger)index
{
    return [[[self data] objectAtIndex:index % NUM_BACKGROUNDS] objectAtIndex:3];
}*/

- (NSString *) getGalleryNameForPicture:(NSInteger)index
{
    return @"New Image Art Gallery";
}

/*
- (NSString *) getGalleryUrlForPicture:(NSInteger)index
{
    return @"http://www.newimageartgallery.com/";
}*/

@end
