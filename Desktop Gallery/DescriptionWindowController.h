//
//  DescriptionWindowController.h
//  Desktop Gallery
//
//  Created by Glenn Thrope on 2/28/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <PhFacebook/PhFacebook.h>
#import "ArtData.h"

@interface DescriptionWindowController : NSWindowController

@property (nonatomic, strong) PhFacebook *fb;
@property (nonatomic, strong) ArtData *artData;

@property (weak) IBOutlet NSTextField *paintingName;
@property (weak) IBOutlet NSTextField *artistName;
@property (weak) IBOutlet NSTextField *galleryName;
@property (weak) IBOutlet NSButton *likeButton;
@property (weak) IBOutlet NSButton *dislikeButton;
@property (weak) IBOutlet NSButton *browseButton;

- (IBAction)likeClicked:(NSButton *)sender;
- (IBAction)dislikeClicked:(NSButton *)sender;
- (IBAction)browseClicked:(NSButton *)sender;

@end
