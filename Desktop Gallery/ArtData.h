//
//  ArtData.h
//  Desktop Gallery
//
//  Created by Glenn Thrope on 2/19/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArtData : NSObject
- (NSString *) getPictureName:(NSInteger)index;
- (NSURL *) getPictureURL:(NSInteger)index;
- (NSURL *) getPictureFile:(NSInteger)index;
- (NSString *) getArtistNameForPicture:(NSInteger)index;
//- (NSString *) getArtistURLForPicture:(NSInteger)index;
- (NSString *) getGalleryNameForPicture:(NSInteger)index;
//- (NSString *) getGalleryUrlForPicture:(NSInteger)index;
@end
